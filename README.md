pour cloner le projet :https://gitlab.com/t.goullet/gsbweb.git

ouvrez un terminal sur le projet et entrez : composer install
puis: cp .env.example .env
puis: php artisan key:generate

-entrer dans le teminal : php artisan migrate:fresh --seed

-puis entrer : php artisan storage:link

dans le terminal : composer require hardevine/shoppingcart
si une erreur implicant la memoire du composer survient passer en super root avec la commande "su -" puis entrez votre mot de passe

une fois en "su -" utiliser la commande "cd / etc" puis "nano php.ini" descendez jusqu'a Ressource limite
puis chercher memory limit et modifier la valeur par -1 afin d'avoir "memory limit = -1" 

puis effectuer cette ligne de commande dans le terminal : php artisan vendor:publish --provider="Gloudemans\Shoppingcart\ShoppingcartServiceProvider" --tag="config" 
cela permet de configurer la base de données

Pour faire les test : php artisan make:test UserTest --unit

admin login : admin@a.fr
admin mot de passe : 123+aze

client login :client@c.fr
client mot de passe : 123+aze
