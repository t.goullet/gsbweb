<!DOCTYPE html>
<html>
@include('include/head')

<body id="page-top">

@include('include.navbar')
<div class="container-fluid">
    <div class="row justify-content-md-center">
        <div class="col-12">
            <form action="{{route("showPanier")}}" class="text-center mt-5 mb-3">
                @csrf
                <div class="form-group">
                    <div class="col-sm-3 my-1">
                        <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Numéro de carte</div>
                            </div>
                            <input type="number" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3 my-1">
                        <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Titulaire de la carte</div>
                            </div>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3 my-1">
                        <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">date d'expiration</div>
                            </div>
                            <input type="number" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3 my-1">
                        <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">cryptogramme</div>
                            </div>
                            <input type="number" class="form-control">
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view_">Confirmer la commande</button>
                    <button type="submit" class="btn btn-primary" onclick="history.go(-1)">Back</button>
                </div>
            </form>

            <form action="{{route("deletePanier")}}" method="POST">
                @csrf
                <div class="modal fade" id="view_">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Confirmer la commande
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" data-toggle="collapse">Valider</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('include.footer')
</body>
</html>

