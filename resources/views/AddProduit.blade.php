<!DOCTYPE html>
<html>
@include('include/head')

<body id="page-top">

@include('include.navbar')

<form action="{{route("DoInsert")}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupUsername"></label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Categorie</div>
                </div>
                <input type="text" class="form-control" name="Categorie" value="{{$produit->ProduitCategorieid}}" readonly>
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupUsername"></label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Prix</div>
                </div>
                <input type="text" class="form-control" name="Prix">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupUsername"></label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Name</div>
                </div>
                <input type="text" class="form-control" name="Nom" >
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupUsername"></label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description</div>
                </div>
                <input type="text" class="form-control" name="Description" >
            </div>
        </div>

        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupUsername"></label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Images</div>
                </div>
                <input type="file" class="form-control" name="image" >
            </div>
        </div>

        <button type="submit" class="btn btn-primary" data-toggle="collapse">Valider</button>
        <button type="button" class="btn btn-primary" onclick="history.go(-1)">Back</button>
    </div>
</form>
@include('include.footer')
</body>
</html>

