<!DOCTYPE html>
<html>
@include('include/head')

<body id="page-top">

@include('include.navbar')

<div class="text-center mb-5 mt-5">
    <div class="titre">Votre Panier</div>
</div>

<div class="row justify-content-md-center">
    <div class="col-10">
        @if(Cart::instance(auth()->user()->name)->count() > 0)
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Nom</th>
                <th scope="col">Quantite</th>
                <th scope="col">Prix</th>
                <th scope="col">supprimer</th>
            </tr>
            </thead>
            <tbody>
            @foreach(Cart::instance(auth()->user()->name)->content() as $produits)
                <tr>
                    <th scope="row">{{$produits->id}}</th>
                    <td>{{$produits->name}}</td>
                    <td>{{$produits->qty}}</td>
                    <td>{{$produits->price}}</td>
                    <td>
                        <form action="{{route("removeProduitPanier", $produits->rowId)}}"method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" data-toggle="modal" data-target="#view_">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                                </svg>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">sous-total</th>
                    <th scope="col">Tax</th>
                    <th scope="col">Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{Cart::subtotal()}}</td>
                    <td>{{Cart::tax()}}</td>
                    <td>{{Cart::total()}}</td>
                </tr>
                </tbody>
            </table>

        @else
            <p>Le panier est vide</p>
        @endif
        <form class="text-center mt-5 mb-3" action="{{route("showCategorie")}}">
            @if(Cart::instance(auth()->user()->name)->count() > 0)
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view_">Passer commande</button>
            @endif
            <button type="submit" class="btn btn-primary" >Retour</button>
        </form>

        <form action="{{route("commande")}}" method="POST">
            @csrf
            <div class="modal fade" id="view_">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            Confirmer la commande
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" data-toggle="collapse">Valider</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
    <div>
    </div>
@include('include.footer')

</body>
</html>
