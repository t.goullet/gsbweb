<!DOCTYPE html>
<html>
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand " href="#page-top"><img src="/assets/img/logo.png" alt="" /></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">

                @if(auth()->user()->roles->permissions->name == "Client")
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{route("showPanier")}}">Panier <span class="badge badge-pill badge-dark">{{Cart::instance(auth()->user()->name)->count()}}</span></a></li>
                @endif

                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{route("showCategorie")}}">Acceuil</a></li>

                @if(auth()->user()->roles->permissions->name == "Admin")
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{route("showViewAdmin")}}">Admin</a></li>
                @endif
                <div class="btn-group">
                    <button class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" data-bs-display="static" aria-expanded="false">
                        {{auth()->user()->name}}
                    </button>
                    <ul class="dropdown-menu dropdown-menu-lg-end">
                        <li><a class="dropdown-item" href="{{route("deconnexion")}}">Déconnexion</a></li>
                    </ul>
                </div>


            </ul>
        </div>
    </div>
</nav>
<!-- Masthead-->
<header class="masthead">
    <div class="container">
        <div class="masthead-subheading">Bienvenue sur GSB-Web</div>
    </div>
</header>

