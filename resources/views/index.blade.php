<!DOCTYPE html>
<html>

@include('include/head')

<body id="page-top">

@include('include.navbar')

@if($message = Session::get('success'))
    <div class="alert alert-success alert-block">

        <button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

    </div>
@endif
<section class="page-section bg-light" id="portfolio">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Categorie de medicament</h2>
            <h3 class="section-subheading text-muted">Toutes les catégories de medicaments référencés</h3>
        </div>
        <div class="row">
            @foreach($categories as $item)
                <div class="col-lg-4 col-sm-6 mb-4">
                    <div class="portfolio-item">
                        <a class="portfolio-link" href="{{route("listeProd", ["categorie"=>$item->id])}}">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src={{$item->CategorieImage}} alt="Image de la catégorie {{$item->CategorieNom}}" />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">{{$item->CategorieNom}}</div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

@include('include.footer')

</body>
</html>
