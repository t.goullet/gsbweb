<!DOCTYPE html>
<html>
@include('include/head')

<body id="page-top">

@include('include.navbar')


<br>


<div class="text-center mb-5">
    <div class="titre">Ajout d'un compte</div>
</div>

<form action="{{route("DoAddUser")}}" method="POST">
    @csrf
    <div class="row justify-content-md-center">
        <div class="col-10">

            <div class="input-group mb-3">
                <span class="input-group-text">Nom :</span>
                <input type="text" class="form-control" name="name">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">Email :</span>
                <input type="email" class="form-control" name="email">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">Mots de passe :</span>
                <input type="password" class="form-control" name="password">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">Confirmer le mots de passe :</span>
                <input type="password" class="form-control" name="password_verify">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" >Roles :</span>
                <select class="form-select" name="permission">
                    <option>Visiteur</option>
                    <option>Client</option>
                    <option>Admin</option>
                </select>
            </div>
        </div>

        <div class="text-center mt-3 mb-5">
            <button type="submit" class="btn btn-primary" data-toggle="collapse">Ajouter l'utilisateur</button>
            <button type="button" class="btn btn-primary" onclick="history.go(-1)">Retour</button>
        </div>

    </div>

</form>

@include('include.footer')
</body>
</html>
