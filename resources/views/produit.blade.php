<!DOCTYPE html>
<html>
@include('include/head')

<body id="page-top">

@include('include.navbar')

<br>
<div class="titre"></div>

<div class="text-center mb-5">
    <img class="drapeau" src=""/>
</div>

<div class="row justify-content-md-center">
    <div class="col-10">

        <table class="table table-sm">
            <tbody>
            <tr>
                <th scope="col">id</th>
                <th>{{$produit->id}}</th>
            </tr>
            <tr>
                <th scope="row">nom</th>
                <td>{{$produit->ProduitNom}}</td>

            </tr>
            <tr>
                <th scope="row">Description</th>
                <td>{{$produit->ProduitDescription}}</td>

            </tr>
            <tr>
                <th scope="row">Prix</th>
                <td colspan="2">{{$produit->ProduitPrixUnitaire}}</td>

            </tr>
            @if(auth()->user()->roles->permissions->name == "Admin")
                <tr>
                    <th scope="row">Edit</th>
                    <td> <a href="{{route("showEdit", ["produit"=>$produit->id])}}"><svg width="1em"
                                                                                         height="1em"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="bi bi-pen-fill"
                                                                                         fill="currentColor"
                                                                                         xmlns="http://www.w3.org/2000/svg"
                                                                                         style="color:green">
                                <path fill-rule="evenodd"
                                      d="M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z"/>
                            </svg></a>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Delete</th>
                    <td>  <a href="" class="text-info" data-toggle="modal" data-target="#view_{{$produit->id}}"><svg width="1em"
                                                                                                                     height="1em"
                                                                                                                     viewBox="0 0 16 16"
                                                                                                                     class="bi bi-trash-fill"
                                                                                                                     fill="currentColor"
                                                                                                                     xmlns="http://www.w3.org/2000/svg"
                                                                                                                     style="color:#ff0000">
                                <path fill-rule="evenodd"
                                      d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                            </svg>
                        </a>
                        <form action="{{route("DoDelete", ["produit"=>$produit->id])}}" method="POST">
                            @csrf
                            <div class="modal fade" id="view_{{$produit->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            Voulez vous supprimez ce produit ?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary" data-toggle="collapse">Valider</button>
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        @if(auth()->user()->roles->permissions->name == "Client")
        <form action="{{route(("AddPanier"))}}" method="POST">
            @csrf
            Quantité : <input type="number" name="quantite" value="1" min="1">
            <input type hidden name="Produit_id" value="{{$produit->id}}">
            <button type="submit" class="btn btn-primary" data-toggle="collapse">Ajouter au panier</button>
        </form>
            @endif

    </div>
</div>
@include('include.footer')
</body>
</html>
