<!DOCTYPE html>
<html>
@include('include/head')

<body id="page-top">

@include('include.navbar')


<br>


<div class="text-center mb-5">
    <div class="titre">Modification du compte</div>
</div>

<form action="{{route("DoEditUser", ["user"=>$user->id])}}" method="POST">
    @csrf
    <div class="row justify-content-md-center">
        <div class="col-10">

            <div class="input-group mb-3">
                <span class="input-group-text">Nom :</span>
                <input type="text" class="form-control" name="name" value="{{$user->name}}"readonly>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">Email :</span>
                <input type="email" class="form-control" name="email" value="{{$user->email}}" readonly>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text" >Roles :</span>
                <select class="form-select" name="permission" >
                    <option @if($user->roles->permissions->name == "Visiteur") selected @endif >Visiteur</option>
                    <option @if($user->roles->permissions->name == "Client") selected @endif >Client</option>
                    <option @if($user->roles->permissions->name == "Admin") selected @endif >Admin</option>
                </select>
            </div>
        </div>

        <div class="text-center mt-5 mb-5">
            <button type="submit" class="btn btn-primary" data-toggle="collapse">Modifier l'utilisateur</button>
            <button type="button" class="btn btn-primary" onclick="history.go(-1)">Retour</button>
        </div>



    </div>
</form>

@include('include.footer')
</body>
</html>
