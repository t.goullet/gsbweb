<!DOCTYPE html>
<html>
@include('include/head')

<body id="page-top">

@include('include.navbar')

<br>


<div class="text-center mb-5">
    <div class="titre">Gérer les comptes des utilisateurs</div>
</div>

<div class="row justify-content-md-center">
    <div class="col-10">

        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Nom</th>
                <th scope="col">Email</th>
                <th scope="col">Roles</th>
                <th scope="col">Ajouter/Modifier</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row"></th>
                <td></td>
                <td></td>
                <td></td>
                <td> <a href="{{route("showViewAddUser")}}"><svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="16" height="16"
                                                                 fill="currentColor"
                                                                 class="bi bi-person-plus-fill"
                                                                 viewBox="0 0 16 16"
                                                                 style="color:blue">
                            <path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                            <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                        </svg></a>
                </td>
            </tr>
            @foreach($user as $users)
            <tr>
                <th scope="row">{{$users->id}}</th>
                <td>{{$users->name}}</td>
                <td>{{$users->email}}</td>
                <td>{{$users->roles->permissions->name}}</td>
                <td> <a href="{{route("showViewEditUser", ["user"=>$users->id])}}"><svg width="1em"
                                                                                         height="1em"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="bi bi-pen-fill"
                                                                                         fill="currentColor"
                                                                                         xmlns="http://www.w3.org/2000/svg"
                                                                                         style="color:green">
                            <path fill-rule="evenodd"
                                  d="M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z"/>
                        </svg></a>
                </td>
            </tr>
            @endforeach

            </tbody>
        </table>

        <form class="text-center mt-5 mb-3">
            <button type="button" class="btn btn-primary" onclick="history.go(-1)">Retour</button>
        </form>

    </div>
</div>
@include('include.footer')
</body>
</html>
