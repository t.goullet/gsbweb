<!DOCTYPE html>
<html>
<head>
    <title>GSB</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#"><img class="card-img-top" src="img/monde.png" alt="Card image cap"></a>
    <a class="navbar-brand" href="#">GSB</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Produits
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Analgésique</a>
                    <a class="dropdown-item" href="#">Antibiotiques</a>
                    <a class="dropdown-item" href="#">Antiviraux</a>
                    <a class="dropdown-item" href="#">Cardiologie</a>
                    <a class="dropdown-item" href="#">Dermatologie</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
        </ul>

        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<br>
<p>Présentation  du site a faire</p>
<br>
<div class="card-deck">
    <div class="row">
       @foreach($categories as $item)
            <div class= "col-lg-4 d-flex justify-content-around">
                <div class="card">
                    <a href="{{route("listeProd", ["categorie"=>$item->id])}}">
                        <img class="card-img-top" src="http://loremflickr.com/400/300/{{$item -> CategorieNom}}/" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$item -> CategorieNom}} </h5>
                            <p class="card-text">appuyer pour aller vers la catégorie voulu</p>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>

</body>
<footer>

</footer>
</html>
