<!DOCTYPE html>
<html>
@include('include/head')

<body id="page-top">

@include('include.navbar')

<section class="page-section bg-light" id="portfolio">
    <div class="container">
        <div class="text-center">
            <h3 class="section-heading text-uppercase">Listes des medicaments pour la catégorie : {{$categories->CategorieNom}} </h3>
            <h3 class="section-subheading text-muted">Description : {{$categories->CategorieDescription}}</h3>
        </div>
        <div class="row">
            @foreach($produits as $item)
                <div class="col-lg-4 col-sm-6 mb-4">
                    <div class="portfolio-item">
                        <a class="portfolio-link" href="{{route("detailProduit", ["produit"=>$item->id])}}">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src={{$item->ProduitsImages}}  alt="Image du produit {{$item->ProduitNom}}" />
                        </a>
                        <div class="portfolio-caption">
                            <a href="{{route("detailProduit", ["produit"=>$item->id])}}"><div class="portfolio-caption-heading">{{$item->ProduitNom}}</div></a>
                          </div>
                    </div>
                </div>
            @endforeach
            @if(auth()->user()->roles->permissions->name == "Admin")
                <div class="col-lg-4 col-sm-6 mb-4">
                    <div class="portfolio-item">
                        <a class="portfolio-link" href="{{route("showInsert", ["categorie"=>$item->ProduitCategorieid])}}">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="/assets/img/portfolio/01-thumbnail.jpg" alt="" />
                        </a>
                        <div class="portfolio-caption">
                            <a href="{{route("showInsert", ["categorie"=>$item->ProduitCategorieid])}}"><div class="portfolio-caption-heading">Ajouter un produit</div></a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div>
        <center>{{$produits->links()}}</center>
    </div>
</section>
@include('include.footer')

</body>
</html>
