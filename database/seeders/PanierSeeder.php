<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PanierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Panier')->insert([
            [
                'PanierDate' => Carbon::now(),
                'PanierUserId' => 1,
            ],
        ]);
    }
}
