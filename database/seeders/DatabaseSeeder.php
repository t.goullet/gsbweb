<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategorieSeeder::class,
            ProduitsSeeder::class,
            EtatSeeder::class,

            PermissionsSeeder::class,


            UsersSeeder::class,

            RolesSeeder::class,

            PanierSeeder::class,
            LignePanierSeeder::class,


        ]);
    }
}
