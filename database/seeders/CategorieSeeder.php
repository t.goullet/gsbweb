<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Categorie')->insert([
            [
                'CategorieNom' => 'Antiasthmatiques',
                'CategorieDescription' => "Relatif à ce qui est propre à combattre et à apaiser l'asthme; l'asthme étant une affection qui se caractérise par des accès de suffocation et est dû à un spasme des bronches ou à leur encombrement par des sécrétions excessives, ou enfin à un épaississement inflammatoire passager de leur muqueuse.",
                'CategorieImage' => '/storage/categorie/Antiasthmatiques.jpg',
            ],
            [
                'CategorieNom' => 'Antibiotique',
                'CategorieDescription' => 'Un antibiotique1 est une substance naturelle ou synthétique qui détruit ou bloque la croissance des bactéries.',
                'CategorieImage' => '/storage/categorie/antibiotiques.jpg',
            ],
            [
                'CategorieNom' => 'Cardiaque',
                'CategorieDescription' => "L'oncologie, carcinologie ou cancérologie est la spécialité médicale d'étude, de diagnostic et de traitement des cancers",
                'CategorieImage' => '/storage/categorie/cardiaque.png',
            ],
            [
                'CategorieNom' => 'Hypnotiques',
                'CategorieDescription' =>  "On appelle hypnotique ou somnifères toute substance capable d'induire et/ou de maintenir le sommeil. Les hypnotiques actuellement disponibles ont en commun une action inhibitrice sur le système nerveux central qui, selon la dose utilisée, entraîne un effet sédatif, le sommeil narcotique ou un coma.",
                'CategorieImage' => '/storage/categorie/hypnotiques.jpg',
            ],
            [
                'CategorieNom' => 'Thymorégulateurs',
                'CategorieDescription' =>  "Les médicaments régulateurs de l’humeur (thymorégulateurs) constituent les traitements de fond du trouble bipolaire. Ils s’appuie essentiellement sur la prescription de médicaments destinés à prévenir les rechutes. C’est un traitement de fond qui sera pris pendant des années, voire toute la vie. Les traitements de fond disponibles aujourd’hui ont apporté un bénéfice considérable aux personnes souffrant de maniaco-dépression.",
                'CategorieImage' => '/storage/categorie/thymorégulateurs.jpg',
            ],
        ]);
    }
}
