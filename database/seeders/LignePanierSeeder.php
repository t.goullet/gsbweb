<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LignePanierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('LignePanier')->insert([
            [
                'LignePanierPanierId' => 1,
                'LignePanierProduitId' => 2,
                'LignePanierQuantite' => 2,
                'LignePanierPrixUnitaire' => 25,
                'LignePanierIdEtat' => 1,
            ],
        ]);
    }
}
