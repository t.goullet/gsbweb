<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Roles')->insert([
            [
                'RolesUsersId' => 1,
                'RolesPermissionsId' => 1,
            ],
            [
                'RolesUsersId' => 2,
                'RolesPermissionsId' => 2,
            ],
            [
                'RolesUsersId' => 3,
                'RolesPermissionsId' => 3,
            ],
            [
                'RolesUsersId' => 4,
                'RolesPermissionsId' => 2,
            ],
            [
                'RolesUsersId' => 5,
                'RolesPermissionsId' => 1,
            ],
        ]);
    }
}
