<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProduitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Produits')->insert([
            [
                'ProduitNom' => 'Bricanyl 2.5 mg/ml',
                'ProduitDescription' => "Traitement symptomatique des asthmes aigus graves",
                'ProduitPrixUnitaire' => '10.84',
                'ProduitsImages' => '/storage/produit/Bricanyl.png',
                'ProduitCategorieid' => 1,
            ],
            [
                'ProduitNom' => 'Formoterol Mylan 12 mg',
                'ProduitDescription' =>  "FORMOTEROL MYLAN est indiqué chez les patients asthmatiques, en association à la corticothérapie inhalée continue de fond, en traitement symptomatique de l'obstruction bronchique et en traitement préventif de l'asthme induit par l'effort, lorsqu'un traitement adapté par corticoïdes s'avère insuffisant.",
                'ProduitPrixUnitaire' => '11.13',
                'ProduitsImages' => '/storage/produit/Formoterol.jpg',
                'ProduitCategorieid' => 1,
            ],
            [
                'ProduitNom' => 'Prednisolone Biogaran 20 mg',
                'ProduitDescription' => "Il est indiqué dans certaines maladies, où il est utilisé pour son effet anti-inflammatoire et son efficacité en cas d'asthme",
                'ProduitPrixUnitaire' => '3.15',
                'ProduitsImages' => '/storage/produit/Prednisolone.jpg',
                'ProduitCategorieid' => 1,
            ],
            [
                'ProduitNom' => 'Dilitrane 100 mg',
                'ProduitDescription' => "Traitement symptomatique continu de l'asthme persistant et des autres bronchopneumopathies chroniques obstructives.",
                'ProduitPrixUnitaire' => '1.67',
                'ProduitsImages' => '/storage/produit/Dilitrane.jpg',
                'ProduitCategorieid' => 1,
            ],
            [
                'ProduitNom' => 'Bronchodual 50 mg',
                'ProduitDescription' => "Traitement symptomatique des exacerbations au cours de la maladie asthmatique ou de la bronchite chronique obstructive lorsqu'il existe une composante réversible.",
                'ProduitPrixUnitaire' => '7.68',
                'ProduitsImages' => '/storage/produit/Bronchodual.jpg',
                'ProduitCategorieid' => 1,
            ],
            [
                'ProduitNom' => 'Oracilline 5 ml',
                'ProduitDescription' => "L' Oracilline (pénicilline V) est un antibiotique bactéricide de la famille des bêtalactamines, du groupe des pénicillines naturelles, du type des phénoxypénicillines.",
                'ProduitPrixUnitaire' => '6.91',
                'ProduitsImages' => '/storage/produit/Oracilline.jpg',
                'ProduitCategorieid' => 2,
            ],
            [
                'ProduitNom' => 'Meropenem Panpharma 1 g',
                'ProduitDescription' => "Elles procèdent de l'activité antibactérienne et des caractéristiques pharmacocinétiques de la Meropenem.",
                'ProduitPrixUnitaire' => '7.67',
                'ProduitsImages' => '/storage/produit/Meropenem.jpg',
                'ProduitCategorieid' => 2,
            ],
            [
                'ProduitNom' => 'Amoxycilline/Acide Clavalanique Zydus 100 mg/12,5 mg/mL',
                'ProduitDescription' =>  "AMOXICILLINE/ACIDE CLAVULANIQUE ZYDUS est indiqué pour le traitement des infections tel que Sinusite bactérienne aiguë, Otite moyenne aiguë, Exacerbation aiguë de bronchite chronique, Pneumonie aiguë communautaire, Cystite, Pyélonéphrite.",
                'ProduitPrixUnitaire' => '4.41',
                'ProduitsImages' => '/storage/produit/Amoxycilline.png',
                'ProduitCategorieid' => 2,
            ],
            [
                'ProduitNom' => 'Ciprofloxacine 250 mg',
                'ProduitDescription' => "Ce médicament est un antibiotique qui appartient à la famille des quinolones. Il est utilisé dans le traitement de diverses maladies infectieuses",
                'ProduitPrixUnitaire' => '6.65',
                'ProduitsImages' => '/storage/produit/Ciprofloxacine.jpg',
                'ProduitCategorieid' => 2,
            ],
            [
                'ProduitNom' => 'Thiophenicol 750 mg',
                'ProduitDescription' => "Thiophenicol procède de l'activité antibactérienne et des caractéristiques pharmacocinétiques du thiamphénicol et tiennent compte de la situation de cet antibiotique dans l'éventail des produits antibactériens actuellement disponibles.",
                'ProduitPrixUnitaire' => '19.98',
                'ProduitsImages' => '/storage/produit/Thiophenicol.png',
                'ProduitCategorieid' => 2,
            ],
            [
                'ProduitNom' => 'Candésartan Arrow 4 mg',
                'ProduitDescription' => "Ce médicament est un antihypertenseur qui appartient à la famille des inhibiteurs de l'angiotensine II. Il bloque l'action de l'angiotensine II. Cette substance, naturellement présente dans l'organisme, provoque une contraction des artères qui augmente la pression artérielle et fatigue le cœur",
                'ProduitPrixUnitaire' => '3.75',
                'ProduitsImages' => '/storage/produit/Candésartan.jpg',
                'ProduitCategorieid' => 3,
            ],
            [
                'ProduitNom' => 'Tareg 160 mg',
                'ProduitDescription' => "Ce médicament est un antihypertenseur qui appartient à la famille des inhibiteurs de l'angiotensine II. Il bloque l'action de l'angiotensine II. Cette substance, naturellement présente dans l'organisme, provoque une contraction des artères qui augmente la pression artérielle et fatigue le cœur.",
                'ProduitPrixUnitaire' => '15.85',
                'ProduitsImages' => '/storage/produit/Tareg.jpg',
                'ProduitCategorieid' => 3,
            ],
            [
                'ProduitNom' => 'Valsartan Zentivvia 40 mg',
                'ProduitDescription' => "Ce médicament est un antihypertenseur qui appartient à la famille des inhibiteurs de l'angiotensine II. Il bloque l'action de l'angiotensine II. Cette substance, naturellement présente dans l'organisme, provoque une contraction des artères qui augmente la pression artérielle et fatigue le cœur.",
                'ProduitPrixUnitaire' => '3.75',
                'ProduitsImages' => '/storage/produit/Valsartan.png',
                'ProduitCategorieid' => 3,
            ],
            [
                'ProduitNom' => 'Cardensiel 1,25 mg',
                'ProduitDescription' => "Ce médicament appartient à la famille des bêtabloquants. Ceux-ci agissent en bloquant l'action de l'adrénaline (et d'autres hormones apparentées) sur de nombreux organes, notamment sur le cœur, les vaisseaux et plus faiblement sur les bronches. A faible dose, les bêtabloquants permettent de protéger le cœur des insuffisants cardiaques.",
                'ProduitPrixUnitaire' => '5.04',
                'ProduitsImages' => '/storage/produit/Cardensiel.jpg',
                'ProduitCategorieid' => 3,
            ],
            [
                'ProduitNom' => 'Nebilox 5 mg',
                'ProduitDescription' => "Ce médicament appartient à la famille des bêtabloquants. Ceux-ci agissent en bloquant l'action de l'adrénaline (et d'autres hormones apparentées) sur de nombreux organes, notamment le cœur, les vaisseaux et les bronches.",
                'ProduitPrixUnitaire' => '6.02',
                'ProduitsImages' => '/storage/produit/Nebilox.jpg',
                'ProduitCategorieid' => 3,
            ],
            [
                'ProduitNom' => 'Lormétazépam EG 1 mg',
                'ProduitDescription' => "Ce médicament est un hypnotique (somnifère) qui appartient à la famille des benzodiazépines. Il est utilisé dans le traitement de l'insomnie.",
                'ProduitPrixUnitaire' => '0.98',
                'ProduitsImages' => '/storage/produit/Lormétazépam.jpg',
                'ProduitCategorieid' => 4,
            ],
            [
                'ProduitNom' => 'Imovane 7,5 mg',
                'ProduitDescription' => "Les indications sont limitées au traitement de courte durée des troubles sévères du sommeil chez l'adulte pour les Insomnies occasionnelle ainsi que les Insomnie transitoire",
                'ProduitPrixUnitaire' => '1.14',
                'ProduitsImages' => '/storage/produit/Imovane.jpg',
                'ProduitCategorieid' => 4,
            ],
            [
                'ProduitNom' => 'Stilinox 10 mg',
                'ProduitDescription' => "Ce médicament est un hypnotique (somnifère) dont les propriétés sont proches de celles des benzodiazépines. Il est utilisé dans le traitement de l'insomnie",
                'ProduitPrixUnitaire' => '1.22',
                'ProduitsImages' => '/storage/produit/Stilinox.jpg',
                'ProduitCategorieid' => 4,
            ],
            [
                'ProduitNom' => 'Zolpidem EG 10 mg',
                'ProduitDescription' => "Ce médicament est un hypnotique (somnifère) dont les propriétés sont proches de celles des benzodiazépines. Il est utilisé dans le traitement de l'insomnie",
                'ProduitPrixUnitaire' => '1.64',
                'ProduitsImages' => '/storage/produit/Zolpidem.jpg',
                'ProduitCategorieid' => 4,
            ],
            [
                'ProduitNom' => 'Zopiclone Zydus 7,5 mg',
                'ProduitDescription' => "Ce médicament est un hypnotique (somnifère) dont les propriétés sont proches de celles des benzodiazépines. Il est utilisé dans le traitement de l'insomnie",
                'ProduitPrixUnitaire' => '1.92',
                'ProduitsImages' => '/storage/produit/Zopiclone.jpg',
                'ProduitCategorieid' => 4,
            ],
            [
                'ProduitNom' => 'Tégrétol 20 mg/ml',
                'ProduitDescription' => "Ce médicament appartient à la famille des anticonvulsivants non barbituriques. Il possède également des propriétés sédatives et agit comme régulateur de l'humeur (thymorégulateur). Il est utilisé dans le traitement de certaines formes d'épilepsie, des troubles bipolaires  et des crises maniaques ainsi que des névralgies rebelles de la face et des douleurs neuropathiques.",
                'ProduitPrixUnitaire' => '3.47',
                'ProduitsImages' => '/storage/produit/Tégrétol.jpg',
                'ProduitCategorieid' => 5,
            ],
            [
                'ProduitNom' => 'Lamictal 200 mg',
                'ProduitDescription' => "Ce médicament est un antiépileptique. Il inhibe la libération du glutamate dans le cerveau, une substance impliquée dans le déclenchement des crises d'épilepsie. Il a également un effet bénéfique chez les personnes souffrant d'un trouble bipolaire, mais son mécanisme d'action est mal connu.",
                'ProduitPrixUnitaire' => '6.38',
                'ProduitsImages' => '/storage/produit/Lamicatal.jpeg',
                'ProduitCategorieid' => 5,
            ],
            [
                'ProduitNom' => 'Abilify 5 mg',
                'ProduitDescription' => "Ce médicament est un neuroleptique dit « atypique ». Certains de ses effets indésirables sont moins marqués que ceux des neuroleptiques classiques. Il a des propriétés antipsychotiques. Il est utilisé dans le traitement de la schizophrénie, dans le cadre d'un trouble bipolaire pour traiter les épisodes maniaques et pour prévenir les récidives.",
                'ProduitPrixUnitaire' => '23.96',
                'ProduitsImages' => '/storage/produit/Abilify.jpg',
                'ProduitCategorieid' => 5,
            ],
            [
                'ProduitNom' => 'Risperdal 1 mg',
                'ProduitDescription' => "Ce médicament est un neuroleptique dit « atypique ». Certains de ses effets indésirables sont moins marqués que ceux des neuroleptiques classiques. Il a des propriétés antipsychotiques. Il est utilisé dans le traitement de la schizophrénie, le traitement des états maniaques associés à un trouble bipolaire, le traitement de courte durée de l'agressivité chez les enfants présentant des troubles graves du comportement et chez les personnes atteintes de la maladie d'Alzheimer.",
                'ProduitPrixUnitaire' => '8.15',
                'ProduitsImages' => '/storage/produit/Risperdal.jpg',
                'ProduitCategorieid' => 5,
            ],
            [
                'ProduitNom' => 'Xeroquel LP 300 mg',
                'ProduitDescription' => "Ce médicament est un neuroleptique dit « atypique ». Certains de ses effets indésirables sont moins marqués que ceux des neuroleptiques classiques. Il a des propriétés antipsychotiques et agit également comme régulateur de l'humeur (thymorégulateur) et antidépresseur. Il est utilisé dans le traitement de certains troubles psychiques : schizophrénie, troubles bipolaires. Il est également utilisé dans le traitement des états dépressifs en complément d'un traitement antidépresseur.",
                'ProduitPrixUnitaire' => '6.61',
                'ProduitsImages' => '/storage/produit/Xeroquel.jpg',
                'ProduitCategorieid' => 5,
            ],
        ]);
    }
}
