<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Produits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Produits', function (Blueprint $table) {
            $table->id();
            $table->string('ProduitNom');
            $table->longText('ProduitDescription');
            $table->double('ProduitPrixUnitaire');
            $table->longText('ProduitsImages')->nullable();
            $table->unsignedBigInteger('ProduitCategorieid');
            $table->foreign('ProduitCategorieid')->references('id')->on('Categorie');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Produits');
    }
}
