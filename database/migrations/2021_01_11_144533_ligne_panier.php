<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LignePanier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LignePanier', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('LignePanierPanierId');
            $table->unsignedBigInteger('LignePanierProduitId');
            $table->double('LignePanierQuantite');
            $table->double('LignePanierPrixUnitaire');
            $table->unsignedBigInteger('LignePanierIdEtat');
            $table->foreign('LignePanierPanierId')->references('id')->on('Panier');
            $table->foreign('LignePanierProduitId')->references('id')->on('Produits');
            $table->foreign('LignePanierIdEtat')->references('id')->on('Etat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LignePanier');
    }
}
