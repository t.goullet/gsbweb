<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Roles', function (Blueprint $table) {
            $table->unsignedBigInteger('RolesUsersId');
            $table->unsignedBigInteger('RolesPermissionsId');
            $table->foreign('RolesUsersId')->references('id')->on('Users');
            $table->foreign('RolesPermissionsId')->references('id')->on('Permissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Roles');
    }
}
