<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory;

    protected $table = 'Roles';
    protected $primaryKey = 'RolesUsersId';
    public $incrementing = false;
    public $timestamps = false;

    public function permissions(){
        return $this->hasOne(Permissions::class, 'id', 'RolesPermissionsId');
    }

    public function user(){
        return $this->hasMany(User::class, 'id', 'RolesUsersId');
    }


}
