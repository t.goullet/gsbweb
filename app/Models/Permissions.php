<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    use HasFactory;

    protected $table = 'Permissions';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    public function roles(){
        return $this->hasMany(Roles::class, 'RolesPermissionsId', 'id');
    }
}
