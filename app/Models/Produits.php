<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produits extends Model
{
    use HasFactory;

    protected $table = 'Produits';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    /**
     * Obtient la catégorie associer aux produits
     */
    public function hadCategorie()
    {
        return $this->hasOne(Categorie::class,'id','ProduitCategorieid');
    }


    /**
     * Obtient le produits associer a une catégorie
     */
    public function comments()
    {
        return $this->hasMany('App\Models\Produits');
    }


}
