<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as BasicAuthenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    use HasFactory;
    use BasicAuthenticatable;

    protected $fillable = ['email', 'password'];

    protected $table ='Users';
    public $timestamps = false;
    public $incrementing = true;

    public function roles(){
        return $this->hasOne(Roles::class, 'RolesUsersId', 'id');
    }

}
