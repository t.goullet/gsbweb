<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "Nom"=> "string | required",
            "Description"=> "string | required",
            "Prix"=> "required| integer",
            "image"=> "mimes:jpeg,jpg,png,gif|max:10000",
            "Categorie"=>"required | integer",

        ];
    }
}
