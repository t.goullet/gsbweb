<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Models\Categorie;
use App\Models\Produits;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProduitsController extends Controller
{
    public function __construct()
    {
    }

    /*
     * Permet d'obtenir tous les produits d'une categorie
     */
    public function showProduits($categorie){


       $produits = Produits::where("ProduitCategorieid", $categorie)->simplePaginate(5);
       $categories = Categorie::find($categorie);


        return view("listeProduits", ["produits" => $produits, "categories" => $categories]);
    }

    /*
     * renvoie vers la page qui permet d'insérer un nouveaux produit
     */
    public function showInsert($categorie){

        $produit = Produits::where("ProduitCategorieid", $categorie)->first();

        return view("AddProduit", ["produit" => $produit]);
    }

    /*
     * Permet de créé le produits et nous renvoie vers la page liste des produits
     */
    public function DoInsert  ( CreateProductRequest $request){

        $produit = new Produits();

        $produit->ProduitNom = $request->Nom;
        $produit->ProduitDescription = $request->Description;
        $produit->ProduitPrixUnitaire = $request->Prix;

        $filename = "/".Storage::putFile('/public/produit', $request->image);
        $produit->ProduitsImages = str_replace("public", "storage", $filename);

        $produit->ProduitCategorieid = $request->Categorie;
        $produit->save();

        return redirect()->route('showCategorie');
    }

    /*
     * permet de supprimer le produit et nous renvoie vers la page liste des produits
     */
    public function DoDelete($id){

        $produit = Produits::find($id);
        $produit->delete();
        return $this->showProduits($produit->ProduitCategorieid);
    }

    /*
     * Permet de récuperer un produit en fonction de son identifiant
     */
    public function findById($id){

        $produit = Produits::find($id);

        return view("produit", ["produit" => $produit]);

    }

    /*
     * Permet d'afficher la page de modification d'un produit
     */
    public function showEdit($id){
        $produit = Produits::find($id);

        return view("editProduit", ["produit"=>$produit]);
    }

    /*
     * Permet de modifier un produit et nous renvoie sur la page liste des produits
     */
    public function DoEdit(Request $request, $id){


//        $validData = $request->validate([
//            "Nom"=>["string"],
//            "Description"=>["string"],
//            "Prix"=>["integer"]
//        ]);

        $produit = Produits::find($id);

        $produit->ProduitNom = $request->get('Nom');
        $produit->ProduitDescription = $request->get('Description');
        $produit->ProduitPrixUnitaire = $request->get('Prix');

        $produit->save();

        return view("produit", ["produit" => $produit]);

    }
}
