<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PanierController extends Controller
{
    public function __construct()
    {
    }

    /*
     * Permet d're rediriger vers la page paiement
     */
    public function confirmCommande(){
        return view ("paiement");
    }

}
