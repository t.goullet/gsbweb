<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DefaultController extends Controller
{
    /**
     * Affichage de tout les catégorie distincte (des produits)
     */
    public function welcome()
    {
        $categorie = DB::select("SELECT DISTINCT CategorieNom FROM Categori");

        $title = "Welcome to the great GSB Demo";
        return view("welcome", ["titre" => $title, "categorie" => $categorie]);
    }
}
