<?php

namespace App\Http\Controllers;

use App\Models\Permissions;
use App\Models\Roles;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class AdminController extends Controller
{
    public function showViewAdmin()
    {
        $admin = User::All();
        return view("Admin", ["user" => $admin]);
    }

    public function showViewAddUser(){
        return view("addUser");
    }

    /*
     * Permet de créé un user et nous renvoie vers la page liste des users
     */
    public function DoAddUser (Request $request){

        $validData = $request->validate([
            "name"=>["required"],
            "email"=>["required"],
            "password"=>["required"],
            "password_verify"=>["required"],
        ]);

        $users = new User();
        $users->name = $validData["name"];
        $users->email = $validData["email"];
        if ($validData["password"]==$validData["password_verify"]){
            $users->password = Hash::make($validData["password"]);
        }
        $users->save();

        $roles = new Roles();
        $roles->RolesUsersId=$users->id;
        if ($request->get('permission')=="Visiteur"){
            $roles->RolesPermissionsId = 3;
        }
        if ($request->get('permission')=="Client"){
            $roles->RolesPermissionsId = 2;
        }

        if ($request->get('permission')=="Admin"){
            $roles->RolesPermissionsId = 1;
        }
        $roles->save();
        return $this->showViewAdmin();
    }

    public function showViewEditUser($id){
        $admin = User::find($id);
        return view("editUser", ["user" => $admin]);
    }




    /*
    * Permet de modifier un produit et nous renvoie sur la page liste des produits
    */
    public function DoEditUser(Request $request, $id){

        $validData = $request->validate([
            "name"=>["required"],
            "email"=>["required"],
        ]);

        $users = User::find($id);

        $users->name = $validData["name"];
        $users->email = $validData["email"];

        $users->save();

        $roles = Roles::find($id);
        $roles->RolesUsersId=$users->id;
        if ($request->get('permission')=="Visiteur"){
            $roles->RolesPermissionsId = 3;
        }
        if ($request->get('permission')=="Client"){
            $roles->RolesPermissionsId = 2;
        }
        if ($request->get('permission')=="Admin"){
            $roles->RolesPermissionsId = 1;
        }
        $roles->save();
        return $this->showViewAdmin();
    }
}
