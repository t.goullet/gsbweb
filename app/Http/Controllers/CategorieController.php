<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use Illuminate\Http\Request;

class CategorieController extends Controller
{


    /*
     * Permet de récuperer dans un tableau toutes les catégorie
     */
    public function showCategorie(){

        $categories = Categorie::All();

        return view("index", ["categories" => $categories]);
    }
}
