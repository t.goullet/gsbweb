<?php

namespace App\Http\Controllers;

use App\Models\Produits;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Panier");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($rowid)
    {

        $user = Auth::user()->name;
        Cart::instance($user)->remove($rowid);
        return view("Panier");
    }

    /**
     *Ajout d'un produit a une cart(panier) selon l'utilisateur connecté
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produit = Produits::find($request->Produit_id);
        $user = Auth::user()->name;
        Cart::instance($user)->add($produit->id, $produit->ProduitNom, $request->quantite, $produit->ProduitPrixUnitaire)
        ->associate('App\Models\Produits');
        Cart::instance($user)->store;

        return redirect()->route('showCategorie')->with('success', 'Le produits a bien été ajouter');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $user = Auth::user()->name;
        Cart::instance($user)->destroy();

        return redirect()->route('showCategorie');
    }
}
