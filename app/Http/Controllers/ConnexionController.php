<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConnexionRequest;
use Illuminate\Support\Facades\Auth;

class ConnexionController extends Controller
{
    public function formulaire(){
        return view('connexion');
    }

    public function traitement(ConnexionRequest $request){

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            return redirect("/categorie");
        }

        if (auth()->user()->roles->permissions->name == "Visiteur") {
            return back()->withErrors([
                'password' => "vous n'avez pas les permissions de vous connecter "
            ]);
        }

        return back()->withErrors([
            'password' => "Le mots de passe est incorrecte"
        ]);
    }
}
