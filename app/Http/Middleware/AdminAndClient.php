<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminAndClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (auth()->user()->roles->permissions->name == "Admin") {
            return $next($request);
        }
        if (auth()->user()->roles->permissions->name == "Client") {
            return $next($request);
        }
        return back();
    }
}
