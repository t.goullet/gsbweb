<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => ''], function () {
    Route::get("/", [\App\Http\Controllers\ConnexionController::class, 'formulaire']);
    Route::post('/', [\App\Http\Controllers\ConnexionController::class, 'traitement']);
});

Route::group(['middleware' => 'CheckUserConnexion'] , function () {
    Route::get("/categorie", [\App\Http\Controllers\CategorieController::class, "showCategorie"]) ->name("showCategorie") ->middleware('IsAdminAndClient');
    Route::get("/deconnexion", [\App\Http\Controllers\UserController::class, "Deconnexion"]) ->name("deconnexion")->middleware('IsAdminAndClient');

    Route::group(['prefix' => 'produit'], function () {
        Route::get("/{categorie?}", [\App\Http\Controllers\ProduitsController::class, "showProduits"]) ->where("categorie", "[0-9]+")->name("listeProd") ->middleware('IsAdminAndClient');
        Route::get("/details/{produit?}", [\App\Http\Controllers\ProduitsController::class, "findById"]) ->where("produit", "[0-9]+")->name("detailProduit")->middleware('IsAdminAndClient');
        Route::get("/showDelete/{produit?}", [\App\Http\Controllers\ProduitsController::class, "showDelete"]) ->name("showDelete")->middleware('IsAdmin');
        Route::get("/showEdit/{produit?}", [\App\Http\Controllers\ProduitsController::class, "showEdit"]) ->name("showEdit")->middleware('IsAdmin');
        Route::get("/showInsert/{categorie?}", [\App\Http\Controllers\ProduitsController::class, "showInsert"]) ->name("showInsert")->middleware('IsAdmin');
        Route::post("/DoDelete/{produit?}", [\App\Http\Controllers\ProduitsController::class, "DoDelete"]) ->name("DoDelete")->middleware('IsAdmin');
        Route::post("/DoEdit/{produit?}", [\App\Http\Controllers\ProduitsController::class, "DoEdit"])->middleware('IsAdmin');
        Route::post("/DoInsert", [\App\Http\Controllers\ProduitsController::class, "DoInsert"]) ->name("DoInsert") ->middleware('IsAdminAndClient');
    });

    Route::group(['middleware' => 'IsClient'] , function () {
        Route::post("/viderPanier", [\App\Http\Controllers\CartController::class, "destroy"]) ->name("deletePanier");
        Route::delete("/supprimerUnProduit/{rowid?}", [\App\Http\Controllers\CartController::class, "delete"]) ->name("removeProduitPanier");
        Route::get("/panier", [\App\Http\Controllers\CartController::class, "index"]) ->name("showPanier");
        Route::post("/panier/ajouter", [\App\Http\Controllers\CartController::class, "store"]) ->name("AddPanier");
        Route::post("/panier/confirmerLaCommande", [\App\Http\Controllers\PanierController::class, "confirmCommande"])->name("commande");
    });

    Route::group(['middleware' => 'IsAdmin'] , function () {
        Route::group(['prefix' => 'admin'], function () {
            Route::get("/", [\App\Http\Controllers\AdminController::class, "showViewAdmin"]) ->name("showViewAdmin");
            Route::get("/showViewEditUser/{user?}", [\App\Http\Controllers\AdminController::class, "showViewEditUser"]) ->name("showViewEditUser");
            Route::get("/showViewAddUser", [\App\Http\Controllers\AdminController::class, "showViewAddUser"]) ->name("showViewAddUser");
            Route::post("/DoAddUser", [\App\Http\Controllers\AdminController::class, "DoAddUser"]) ->name("DoAddUser");
            Route::post("/DoEditUser/{user?}", [\App\Http\Controllers\AdminController::class, "DoEditUser"]) ->name("DoEditUser");
        });
    });

});
